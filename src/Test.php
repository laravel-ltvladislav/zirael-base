<?php

namespace Zirael\ZiraelBase;
class Test
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
